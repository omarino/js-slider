import { Movie } from "../models/movie";

export function getMovies(s, cb) {
  var xhr = new XMLHttpRequest();
  xhr.open("GET", "https://fake-movie-database-api.herokuapp.com/api?s=" + s);

  xhr.addEventListener("loadend", function () {
    var data = JSON.parse(xhr.response);

    var movies = data.Search.map(function (m) {
      return new Movie(m.imdbID, m.Title, m.Poster);
    });

    cb(movies);
  });

  xhr.send();
}

export function sliceMovies(movies, from, to) {
  var idx = from;
  var goOn = true;
  var ret = [];

  do {
    ret.push(movies[idx]);

    if (idx < to || (idx > to && idx < movies.length)) {
      idx++;
      if (idx === movies.length) {
        idx = 0;
      }
    } else {
      goOn = false;
    }
    // if (idx === to) {
    //   goOn = false;
    // } else if (idx < to || (idx > to && idx < movies.length - 1)) {
    //   idx++;
    // } else {
    //   idx = 0;
    // }
    // ret.push(movies[idx]);

    // if (idx < to) {
    //   idx++;
    //   if (idx > to) {
    //     idx = 0;
    //   }
    // } else {
    //   goOn = false;
    // }
  } while (goOn);

  return ret;
}
