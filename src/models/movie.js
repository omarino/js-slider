// S5
// export function Movie(id, name, image) {
//   this.id = id;
//   this.name = name;
//   this.image = image;
// }

// in S6
export class Movie {
  //   nomeMetodo() {
  //   }
  constructor(id, name, image) {
    this.id = id;
    this.name = name;
    this.image = image;
  }

  getPosterTag() {
    // return '<img src="' + this.image + '">';
    // return '<img src="' .concat(this.image, '">');

    // Backtick (template literal)
    // Permette di interpolare dei valori all' interno della stringa
    // -> il valore da interpolare deve essere declinato all' interno di ${}
    // -> Nelle tastiere US inil. Il carattere backtick si trova subito sotto ad esc
    return `<img class="poster" src="${this.image}">`; // ${} dice che c'è un valore da interpolare
  }
}
