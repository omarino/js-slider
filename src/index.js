// entry point
import "bootstrap/dist/js/bootstrap";
import { getMovies, sliceMovies } from "./functions/slider";
import { getUsers } from "./functions/users";

getUsers(function (users) {
  users.forEach(function (user) {
    tbody.innerHTML =
      "<tr><td>" +
      user.id +
      "</td>" +
      user.email +
      "</td><td>" +
      user.username +
      "</td></tr>";
  });
});

function handleSliderChange(isNext) {
  if (isNext) {
    from = from === movies.length - 1 ? 0 : ++from;
    to = to === movies.length - 1 ? 0 : ++to;
  }
  // if (isNext) {
  //   from = from === movies.length - 1 ? 0 : --from;
  //   to = to === movies.length - 1 ? 0 : --to;
  // }
  else {
    from = from === 0 ? movies.length - 1 : --from;
    to = to === 0 ? movies.length - 1 : --to;
  }

  slider.innerHTML = "";
  sliceMovies(movies, from, to).forEach(function (movie) {
    slider.innerHTML +=
      '<div class="col-lg-3"><img class="poster" src="' +
      movie.image +
      '"></div>';
  });
}
let from = 0;
let to = 3;
let movies = [];

let sel = document.querySelector('select[name="film"]');
// s === "0" ? changeMovies("batman") : s;
let slider = document.getElementById("slider");

changeMovies("batman");

sel.addEventListener("change", function (e) {
  let s = e.target.value; //target è l'elemento che scatena/subisce l'evento
  from = 0;
  to = 3;
  if (s === "0") {
    changeMovies("batman");
  } else {
    changeMovies(s);
  }
});
let tbody = document.getElementById("tbody");

function changeMovies(movie) {
  slider.innerHTML = "";

  getMovies(movie, function (apiMovies) {
    movies = apiMovies;

    // sliceMovies(movies, from, to).forEach(function (movie) {
    //   slider.innerHTML +=
    //     '<div class="col-lg-3"><img class="poster" src="' +
    //     movie.image +
    //     '"></div>';
    // });

    const slices = sliceMovies(movies, from, to);
    // for ('elemento su cui itero' of 'array')
    for (const slice of slices) {
      slider.innerHTML += `
        <div class="col-lg-3">
          ${slice.getPosterTag()}
        <div>
      `;
      //'<div class="col-lg-3">' + slice.getPosterTag() + "</div>";
    }
  });
}

document.querySelectorAll("button.slider-action").forEach(function (b) {
  b.addEventListener("click", function (e) {
    handleSliderChange(e.target.id === "slider-next");
  });
});
// document.querySelectorAll("button.slider-action").forEach(function (c) {
//   c.addEventListener("click", function (d) {
//     handleSliderChange(d.target.id === "slider-next");
//   });
// });

document.addEventListener("keydown", function (e) {
  e.preventDefault();
  if (e.key === "ArrowRight") {
    handleSliderChange(true);
  } else if (e.key === "ArrowLeft") {
    handleSliderChange(false);
  }
});

// for-in
const o = {
  k1: "valore",
  k2: 10,
  chiappa2: {},
};
// of per gli array in per gli oggetti
// const key conterrà la chiave che di volta in volta viene analizzata dal for-in
for (const key in o) {
  // o.key => acceddo alla proprietà key nell'oggetto "o"
  // NON è corretta perchè la constante key non viene interpellata +

  // accedo alla chiave presente in "key" (la prima volta key conterrà k1, le seconda volta k2, la terza chiappa2...)
  // all'interno dell' oggetto "o"
  o[key];
  console.log(o[key]);
  console.log(key, o[key]);
}

Object.keys(o).forEach(function (key) {
  console.log(key, o[key]);
});
// in (operator)
const o2 = {
  k1: undefined,
  alessandroBorghese: 10,
};
// questo sistema non è valido perchè non controllo se la chiave esiste ma ne sto prendendo il valore booleano all'interno (quindi se è true
// svolgi la if altrimenti no)

// if (o2.k1) {
//   console.log("k1 esiste");
// }

// if (o2.alessandroBorghese) {
//   console.log("alessandroBorghese esiste");
// }

// anche una chiave che non esiste in un oggetto restituisce "undefined", come spesso accade in js quando si tenta di accedere a degli
// elementi inesistenti

// if (o2.chiaveCheNonEsiste) {
//}

if ("k1" in o2) {
  console.log("k1 esiste");
}

if ("alessandroBorghese" in o2) {
  console.log("alessandroBorghese esiste");
}
